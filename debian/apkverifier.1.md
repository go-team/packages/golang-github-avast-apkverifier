# NAME

apkverifier - verify the signature on an Android APK

# SYNOPSIS

Command line tool for verifying Android APK files:

**apkverifier** _input.apk_

_input.apk_ is an existing file with an APK Signature

# DESCRIPTION

Simple command line tool that verifies the signatures on Android APK
binaries.  It can verify APK Signature scheme v1, v2 and v3 and passes
Google apksig's testing suite.  It should support all algorithms and
downgrade attack protection.

Because Android can handle even broken x509 cerficates and ZIP files,
apkverifier is using the ZipReader from apkparser package and vendors
crypto/x509 in internal/x509andr and github.com/fullsailor/pkcs7
(https://github.com/fullsailor/pkcs7) in the fullsailor/pkcs7 folder.
The last two have some changes to handle some
not-entirely-according-to-spec certificates.

# SEE ALSO

Documentation on GoDoc (https://godoc.org/github.com/avast/apkverifier)
